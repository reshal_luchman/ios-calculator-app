//
//  calculatorViewModel.swift
//  Calculator
//
//  Created by Reshal Luchman on 2019/02/06.
//  Copyright © 2019 Reshal Luchman. All rights reserved.
//

import Foundation

class CalculatorViewModel
{
    
    var calculateModel = calculatorModel()
    
    func storeFirstNumber(firstNumber: String) {
        calculateModel.number1 = Int(firstNumber)!
        print(calculateModel.number1)

    }
    
    func storeSecondNumber(secondNumber: String) {
        calculateModel.number2 = Int(secondNumber)!
        print(calculateModel.number2)
    }
    
    func calculateNumbers() -> Int {
        return calculateModel.number1 + calculateModel.number2
    }
}
