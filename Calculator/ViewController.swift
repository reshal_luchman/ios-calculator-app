//
//  ViewController.swift
//  Calculator
//
//  Created by Reshal Luchman on 2019/02/06.
//  Copyright © 2019 Reshal Luchman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var calculatorVM = CalculatorViewModel()

    @IBOutlet weak var equationLabel: UILabel!
    @IBOutlet weak var numbersLabel: UILabel!
    
    @IBOutlet var numberButtons: [UIButton]!
    @IBAction func numberButton(_ sender: UIButton) {
        updateNumbersLabel(on: sender)
    }
    
    @IBAction func operationsButton(_ sender: UIButton) {
        
        /*If the current value in the view is retrieved, sent for processing, and then updated to a (another) new value in the view. Is it correct to implement sending the current value for processing like so:*/
        let firstNumber = updateEquationLabel()
        equationLabel.text?.append(" + ")
        calculatorVM.storeFirstNumber(firstNumber: firstNumber)
    }
    
    @IBAction func equalsButton(_ sender: UIButton) {
        
        let secondNumber = updateEquationLabel()
        equationLabel.text?.append(" = ")
        calculatorVM.storeSecondNumber(secondNumber: secondNumber)
        let answer = calculatorVM.calculateNumbers()
        numbersLabel.text = String(answer)
    }
    
    func updateNumbersLabel(on button: UIButton) {
        var numbersLabelTitle = numbersLabel.text!
        var convertedNumberToString = ""
        
        if let buttonTitle = button.currentTitle {
            numbersLabelTitle.append(buttonTitle)
            convertedNumberToString = String(Int(numbersLabelTitle)!)
        }
        numbersLabel.text = convertedNumberToString
    }
    
    func updateEquationLabel() -> String {
        //Is there a better way to do this? get and set?
        var getNumber = ""
        if let getNumberFromTitle = numbersLabel.text {
            equationLabel.text?.append(getNumberFromTitle)
            numbersLabel.text = "0"
            getNumber = getNumberFromTitle
        }
//        better to return a number? Or create a separate function to get the number for calculation. This method should only be used to update the view.
        return getNumber
    }
}

